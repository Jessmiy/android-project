package com.example.androidproject.ui.home;

public class ImageViewItem {

    private int image;

    public ImageViewItem() {

    }

    public ImageViewItem(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }


}