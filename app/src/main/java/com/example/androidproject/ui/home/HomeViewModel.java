package com.example.androidproject.ui.home;

import android.os.Build;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private int image;
    private String title;
    private String desc;

    public HomeViewModel(int image, String title, String desc) {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
        this.image = image;
        this.title = title;
        this.desc = desc;
    }

    public LiveData<String> getText() {
        return mText;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}