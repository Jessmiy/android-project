package com.example.androidproject.ui.home;

public class TextViewItem {

    private String title;

    public TextViewItem() {

    }

    public TextViewItem(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}