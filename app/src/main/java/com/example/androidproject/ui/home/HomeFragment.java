package com.example.androidproject.ui.home;

import android.animation.ArgbEvaluator;
import android.os.Bundle;
import android.os.Handler;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.androidproject.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment {
    private HomeViewModel homeViewModel;
    FirebaseFirestore db;
    ViewPager viewPager;
    SliderAdapter sliderAdapter;
    ImageViewAdapter imageViewAdapter;
    TextViewAdapter textViewAdapter;

    Integer[] colors = null;
    //Integer[] colors_temp = {};
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();
    //ViewFlipper v_flipper;
    private List<Slide> slideList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false); //@#2: Because of using fragment instead of main activity, there will be a "root" needed in order to access other properties..
        final TextView textView = root.findViewById(R.id.text_home); //For instance, accessing text accessible with root
        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        final List<SliderItem> slide_items = new ArrayList<>();
        final List<ImageViewItem> imageView_item = new ArrayList<>();
        final List<TextViewItem> textView_item = new ArrayList<>();

        //Retrieval data of slider item
        final int image_id[] = {R.drawable.hp_slider_1, R.drawable.hp_slider_2, R.drawable.hp_slider_3, R.drawable.hp_slider_4};
        final int background_color_id[] = {R.color.color_slide_1, R.color.color_slide_2, R.color.color_slide_3, R.color.color_slide_4};
        db = FirebaseFirestore.getInstance();
        for(int i = 1; i <= 4; i++) {
            final DocumentReference docRef_slider_item = db.collection("home_slider_item").document("slider_item_" + i);
            docRef_slider_item.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Log.d("success", "DocumentSnapshot data: " + document.getData());
                            String imageIdValue = document.get("image_id") + "";
                            int image = Integer.parseInt(imageIdValue);
                            String title = document.get("title") + "";
                            String desc = document.get("description") + "";
                            //String backgroundColorValue = document.get("background_color_id") + "";
                            //int background_color = Integer.parseInt(backgroundColorValue);

                            slide_items.add(new SliderItem(image_id[image], title, desc));
                            //colors_temp[background_color] = getResources().getColor(background_color_id[background_color]);
                        } else {
                            Log.d("fail", "No such document");
                        }
                    } else {
                        Log.d("fail", "get failed with ", task.getException());
                    }
                }
            });
        }

        //models.add(new Model(R.drawable.hp_slider_1, "Christmas 2019", "Come one, come all!  Everyone is invited to Santa’s Toyland, happening at Mid Valley Megamall! Enjoy the sights and sounds of Christmas while shopping for seasonal gifts. You can even sit in workshops and learn to make festive treats. It’s fun for the entire family this Christmas!  Santa’s Toyland proudly brought to you by Mid Valley Megamall and HSBC."));
        //models.add(new Model(R.drawable.hp_slider_2, "Planning for a trip here? Let us make it easier for you with our Car Park Availability Live Update! Find out exactly how many parking spots are available in real time as our data is updated every 10 seconds."));
        //models.add(new Model(R.drawable.hp_slider_3, "You will be spoilt for choice when it comes to selecting your accommodation for your Southeast Asia travel destinations. With a service strategy of providing guests with a ‘Totally Fulfilling Experience’, guests can be assured of warm hospitality, friendly and efficient service throughout their stay."));
        //models.add(new Model(R.drawable.hp_slider_4, "Hello Johor!", "The Mall, Mid Valley Southkey is now live! #midvalleysouthkey"));

        sliderAdapter = new SliderAdapter(slide_items, this);

        viewPager = viewPager.findViewById(R.id.viewPager);
        viewPager.setAdapter(sliderAdapter);
        viewPager.setPadding(130, 0, 130, 0);

        Integer[] colors_temp = {
                getResources().getColor(background_color_id[0]),
                getResources().getColor(background_color_id[1]),
                getResources().getColor(background_color_id[2]),
                getResources().getColor(background_color_id[3]),
        };

        colors = colors_temp;

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position < (sliderAdapter.getCount() -1) && position < (colors.length - 1)) {
                    viewPager.setBackgroundColor(

                            (Integer) argbEvaluator.evaluate(
                                    positionOffset,
                                    colors[position],
                                    colors[position + 1]
                            )
                    );
                }

                else {
                    viewPager.setBackgroundColor(colors[colors.length - 1]);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //Retrieval data of imageView item
        final int image_id_imageView[] = {R.drawable.deals_1, R.drawable.deals_2, R.drawable.shop_1, R.drawable.shop_2, R.drawable.parking_1};
        for(int i = 1; i <= 5; i++) {
            final DocumentReference docRef_imageView_item = db.collection("imageView_item").document("imageView_item_" + i);
            docRef_imageView_item.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Log.d("success", "DocumentSnapshot data: " + document.getData());
                            String imageIdValue = document.get("image_id") + "";
                            int image = Integer.parseInt(imageIdValue);
                            imageView_item.add(new ImageViewItem(image_id_imageView[image]));
                        } else {
                            Log.d("fail", "No such document");
                        }
                    } else {
                        Log.d("fail", "get failed with ", task.getException());
                    }
                }
            });
        }
        imageViewAdapter = new ImageViewAdapter(imageView_item, this);
        viewPager.setAdapter(imageViewAdapter);

        //Retrieval data of textView item
        for(int i = 1; i <= 5; i++) {
            final DocumentReference docRef_textView_item = db.collection("textView_item").document("textView_item_" + i);
            docRef_textView_item.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Log.d("success", "DocumentSnapshot data: " + document.getData());
                            String title = document.get("title") + "";
                            textView_item.add(new TextViewItem(title));
                        } else {
                            Log.d("fail", "No such document");
                        }
                    } else {
                        Log.d("fail", "get failed with ", task.getException());
                    }
                }
            });
        }
        textViewAdapter = new TextViewAdapter(textView_item, this);
        viewPager.setAdapter(textViewAdapter);
 /*     <ViewFlipper
        android:id="@+id/v_flipper"
        android:layout_width="match_parent"
        android:layout_height="200dp"
        android:layout_centerHorizontal="true" />

       int images[] = {R.drawable.hp_slider_1, R.drawable.hp_slider_2, R.drawable.hp_slider_3, R.drawable.hp_slider_4, R.drawable.hp_slider_5};

        v_flipper = root.findViewById(R.id.v_flipper);

        /*
        //for loop
        for(int i =0 ;i < images.length; i++) {
            flipperImages(images[i]);
        }

        //foreach
        for(int image: images) {
            flipperImages(image);
        }
*/
        return root;
    }

    /*
    public void flipperImages(int image) {
        ImageView imageView = new ImageView(getContext()); //@#2: Instead of using "this" in the main activity, getContext() will be used for the fragment
        imageView.setBackgroundResource(image);

        v_flipper.addView(imageView);
        v_flipper.setFlipInterval(4000); //4 sec per slide
        v_flipper.setAutoStart(true);

        //Animation
        v_flipper.setInAnimation(getContext(), android.R.anim.slide_in_left);
        v_flipper.setInAnimation(getContext(), android.R.anim.slide_in_left);
    }
  */



}