package com.example.androidproject.ui.park;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.androidproject.R;

public class ParkFragment extends Fragment {

    private ParkViewModel parkViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        parkViewModel =
                ViewModelProviders.of(this).get(ParkViewModel.class);
        View root = inflater.inflate(R.layout.fragment_park, container, false);
        final TextView textView = root.findViewById(R.id.text_tools);
        parkViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}